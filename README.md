# momo-store application

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

Все репозитории, относящиеся к momo-store, объединены в [группе](https://gitlab.com/momo-store-group):

- [Репозиторий с фронтом и бэком](https://gitlab.com/momo-store-group/application) - текущий
- [Репозиторий с helm-чартами](https://gitlab.com/momo-store-group/helm-charts)
- [Репозиторий с инфраструктурой](https://gitlab.com/momo-store-group/infrastructure)

<details>
<summary>Зачем использована группа?</summary>

- Сделать код более организованным
- Разделить версии приложения и инфрастуктуры
    - Хэш коммита в main ветке application - версия актуального образа
    - Хэш коммита в main ветке infrastructure - версия актуальной инфраструктуры
- Разделить секреты между репозиториями
- Использовать [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/),
  чтобы не упираться в лимит загрузок образов

</details>

## Install

### Frontend

```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

### Backend

```bash
go run ./cmd/api
go test -v ./... 
```

## Repo

Структура репозитория application:

```
application/
├── backend/ - директория с файлами бэкенда
│   ├── cmd/
│   ├── internal/
│   ├── Dockerfile - файл с описанием сборки образа бэка
│   ├── .gitlab-ci.yaml - пайплайн для сборки и деплоя бэка
│   ├── go.mod
│   └── go.sum
├── frontend/ - директория с файлами фронтенда
│   ├── public/
│   ├── src/
│   ├── babel.config.js
│   ├── .browserslistrc
│   ├── Dockerfile - файл с описанием сборки образа фронта
│   ├── .gitlab-ci.yaml - пайплайн для сборки и деплоя фронта
│   ├── momo-store.conf - дефолтный NGINX конфиг, пакуемый в образ фронта
│   ├── package.json
│   ├── package-lock.json
│   ├── tsconfig.json
│   └── vue.config.js
├── .gitignore
├── .gitlab-ci.yaml - пайплайн с триггерами
└── README.md
```

## CI

Входные данные - новый коммит в любой ветке.

Результат - образ приложения с изменениями из коммита, запушенный в GitLab Container Registry.

### Pipeline

1. Проверить Dockerfile - hadolint
2. Проверить код на уязвимости - SonarQube
3. Выполнить модульные тесты (реализовано только для бэка) - тестовый фреймворк языка
4. Собрать образ - Docker
5. Запушить собранный образ в приватный registry - Docker
    - **Версия образа** - хэш коммита, из которого собран образ
    - Любой успешно собранный образ будет запушен в registry
    - Даже если образ не прошел тесты, то он может понадобиться для дебага
6. Протестировать собранный образ - Postman

## CD

Входные данные - новый коммит в ветке main.

Результат - образ, собранный в CI, развернут в Kubernetes кластере.

### Pipeline

1. Обновить тег образа, используемый в кластере - ArgoCD

### Description

Развертывание выполняется с кнопки => continuous delivery.

- Цена ошибки - неработающий компонент приложения
- Решение - отредактировать тег образа в values и повторить синхронизацию

Фронт и бэк могут быть развернуты независимо друг от друга.

Переменные с секретами, используемые на этапе развертывания, доступны только из protected веток.

Кластер и namespace, в который выполняется деплой, задаются в ArgoCD в настройках приложения.

Фронт принимает запросы от путей, начинающихся с `/momo-store`, не перекрывая другие пути.

## Workflow

1. Создать feature-ветку
2. Запушить изменения в feature-ветку
3. В пайплайне проверить измененный код приложения и собрать его образ
4. Создать MR в main
5. После принятия MR автоматически развернуть новую версию приложения в кластере

## References

- [Мануал](https://v2.vuejs.org/v2/cookbook/dockerize-vuejs-app.html) для написания Dockerfile фронта
- [Объяснение](https://github.com/vuejs/vue-cli/issues/5107#issuecomment-586701382),
  почему `NODE_ENV=production` не используется для установки зависимостей
- [Мануал для написания Dockerfile бэка](https://docs.docker.com/language/golang/build-images/)
- [Статья](https://docs.docker.com/language/java/run-tests/), откуда взята идея с тестированием бэка через Dockerfile

## TODO

- Continuous deployment
    - Если после синхронизации приложение не может стать Healthy, должен автоматически выполняться откат
    - [Пример команды](https://github.com/argoproj/argo-cd/discussions/8452#discussioncomment-2166859),
      чтобы забрать данные приложения (можно сохранить старый тег до замены)
